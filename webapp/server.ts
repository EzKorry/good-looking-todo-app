import express from "express";
import next from "next";
import { createProxyMiddleware } from "http-proxy-middleware";

const port = process.env.PORT || 3000;
const dev = process.env.NODE_ENV !== "production";
const app = next({ dev });
const handle = app.getRequestHandler();

app
  .prepare()
  .then(() => {
    const server = express();

    // if (isDevelopment) {
    //   server.use("/api", createProxyMiddleware(apiPaths["/api"]));
    //   server.use("/api2", createProxyMiddleware(apiPaths["/api2"]));
    // }
    server.use(
      "/api/graphql",
      createProxyMiddleware({
        target: "http://localhost:4000",
        changeOrigin: true,
      })
    );

    server.all("*", (req, res) => {
      return handle(req, res);
    });

    server.listen(port, () => {
      console.log(`> Ready on http://localhost:${port}`);
    });
  })
  .catch((err) => {
    console.log("Error:::::", err);
  });
