import { sign, verify } from "../auth/jwt";
import { expect } from "chai";

describe("jwt", function () {
  describe("sign", function () {
    it("잘 동작해야 함", async function () {
      const signed = sign("hello world!");
      expect(signed).to.be.a("string");
    });
  });
  describe("verify", function () {
    it("잘 동작해야 함", async function () {
      const fixture = "hello world!";
      const signed = sign(fixture);
      const decoded = await verify(signed);

      expect(decoded.data).equal(fixture);
    });
  });
});
