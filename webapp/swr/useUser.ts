import { useSession } from "next-auth/client";
import useSWR from "swr";

export default function useUser() {
  const [session, loading] = useSession();
  useSWR;
}
