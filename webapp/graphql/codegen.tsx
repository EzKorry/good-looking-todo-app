/* eslint-disable @typescript-eslint/explicit-module-boundary-types */
import { gql } from "@apollo/client";
import * as Apollo from "@apollo/client";
export type Maybe<T> = T | null;
export type Exact<T extends { [key: string]: unknown }> = {
  [K in keyof T]: T[K];
};
export type MakeOptional<T, K extends keyof T> = Omit<T, K> &
  { [SubKey in K]?: Maybe<T[SubKey]> };
export type MakeMaybe<T, K extends keyof T> = Omit<T, K> &
  { [SubKey in K]: Maybe<T[SubKey]> };
const defaultOptions = {};
/** All built-in and custom scalars, mapped to their actual values */
export interface Scalars {
  ID: string;
  String: string;
  Boolean: boolean;
  Int: number;
  Float: number;
  /** Mongodb ObjectId */
  ObjectId: any;
}

export interface IMutationRoot {
  __typename?: "MutationRoot";
  createUser: IUser;
}

export interface IMutationRootCreateUserArgs {
  newUser: INewUserInput;
}

/** 사용자 생성 입력 */
export interface INewUserInput {
  email: Scalars["String"];
  nickname: Scalars["String"];
  consent: Scalars["Boolean"];
  role: IUserRole;
}

export interface IQueryRoot {
  __typename?: "QueryRoot";
  user?: Maybe<IUserMongoDb>;
}

export interface IQueryRootUserArgs {
  email: Scalars["String"];
}

/** 앱을 사용하는 사용자입니다. */
export interface IUser {
  __typename?: "User";
  email: Scalars["String"];
  nickname: Scalars["String"];
  consent: Scalars["Boolean"];
  role: IUserRole;
}

export interface IUserMongoDb {
  __typename?: "UserMongoDB";
  id: Scalars["ObjectId"];
  email: Scalars["String"];
  nickname: Scalars["String"];
  consent: Scalars["Boolean"];
  role: IUserRole;
}

export const IUserRole = {
  Admin: "ADMIN",
  Member: "MEMBER",
} as const;

export type IUserRole = typeof IUserRole[keyof typeof IUserRole];
export type IGetUserQueryVariables = Exact<{
  userEmail: Scalars["String"];
}>;

export type IGetUserQuery = {
  __typename?: "QueryRoot";
  user?: Maybe<{
    __typename?: "UserMongoDB";
    email: string;
    nickname: string;
    consent: boolean;
    role: IUserRole;
  }>;
};

export type ICreateUserMutationVariables = Exact<{
  input: INewUserInput;
}>;

export type ICreateUserMutation = {
  __typename?: "MutationRoot";
  createUser: {
    __typename?: "User";
    email: string;
    nickname: string;
    consent: boolean;
    role: IUserRole;
  };
};

export const GetUserDocument = gql`
  query getUser($userEmail: String!) {
    user(email: $userEmail) {
      email
      nickname
      consent
      role
    }
  }
`;

/**
 * __useGetUserQuery__
 *
 * To run a query within a React component, call `useGetUserQuery` and pass it any options that fit your needs.
 * When your component renders, `useGetUserQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useGetUserQuery({
 *   variables: {
 *      userEmail: // value for 'userEmail'
 *   },
 * });
 */
export function useGetUserQuery(
  baseOptions: Apollo.QueryHookOptions<IGetUserQuery, IGetUserQueryVariables>
) {
  const options = { ...defaultOptions, ...baseOptions };
  return Apollo.useQuery<IGetUserQuery, IGetUserQueryVariables>(
    GetUserDocument,
    options
  );
}
export function useGetUserLazyQuery(
  baseOptions?: Apollo.LazyQueryHookOptions<
    IGetUserQuery,
    IGetUserQueryVariables
  >
) {
  const options = { ...defaultOptions, ...baseOptions };
  return Apollo.useLazyQuery<IGetUserQuery, IGetUserQueryVariables>(
    GetUserDocument,
    options
  );
}
export type GetUserQueryHookResult = ReturnType<typeof useGetUserQuery>;
export type GetUserLazyQueryHookResult = ReturnType<typeof useGetUserLazyQuery>;
export type GetUserQueryResult = Apollo.QueryResult<
  IGetUserQuery,
  IGetUserQueryVariables
>;
export const CreateUserDocument = gql`
  mutation createUser($input: NewUserInput!) {
    createUser(newUser: $input) {
      email
      nickname
      consent
      role
    }
  }
`;
export type ICreateUserMutationFn = Apollo.MutationFunction<
  ICreateUserMutation,
  ICreateUserMutationVariables
>;

/**
 * __useCreateUserMutation__
 *
 * To run a mutation, you first call `useCreateUserMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useCreateUserMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [createUserMutation, { data, loading, error }] = useCreateUserMutation({
 *   variables: {
 *      input: // value for 'input'
 *   },
 * });
 */
export function useCreateUserMutation(
  baseOptions?: Apollo.MutationHookOptions<
    ICreateUserMutation,
    ICreateUserMutationVariables
  >
) {
  const options = { ...defaultOptions, ...baseOptions };
  return Apollo.useMutation<ICreateUserMutation, ICreateUserMutationVariables>(
    CreateUserDocument,
    options
  );
}
export type CreateUserMutationHookResult = ReturnType<
  typeof useCreateUserMutation
>;
export type CreateUserMutationResult =
  Apollo.MutationResult<ICreateUserMutation>;
export type CreateUserMutationOptions = Apollo.BaseMutationOptions<
  ICreateUserMutation,
  ICreateUserMutationVariables
>;
