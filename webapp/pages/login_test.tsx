// import { signIn, signOut, useSession } from "next-auth/client";
import React from "react";
import useSWR from "swr";
import Link from "next/link";
import client from "../apollo-client";
import {
  GetUserDocument,
  IGetUserQuery,
  IGetUserQueryVariables,
  useGetUserQuery,
} from "../graphql/codegen";
import { config } from "../util/config";
import { JwtVerified, verify } from "../auth/jwt";
import { google } from "googleapis";
import { NextPage, NextPageContext } from "next";
import Cookie from "universal-cookie";

async function isLogin(jwtToken: string): Promise<boolean> {
  let payload: JwtVerified;
  try {
    payload = await verify(jwtToken);
  } catch (err) {
    console.error("error while verifying jwt");
    return false;
  }
  console.log(payload);
  if (!payload.data) {
    console.error("jwt payload data not found");
    return false;
  }
  const client = new google.auth.OAuth2(
    config.AUTH_GOOGLE_ID,
    config.AUTH_GOOGLE_SECRET,
    "http://localhost:3000/api/auth/callback/google"
  );
  client.setCredentials({
    refresh_token: payload.data,
  });
  const accessToken = await client.getAccessToken();
  if (!accessToken) {
    console.error("accessToken not found");
    return false;
  }
  return true;
}

export async function getServerSideProps(context: NextPageContext) {
  const cookie = new Cookie(context.req?.headers.cookie);
  const token: string = cookie.get(config.AUTH_COOKIE_NAME);
  console.log(`token: ${token}`);
  const islogin = await isLogin(token);
  console.log(`islogin: ${islogin}`);
  return {
    props: { islogin }, // will be passed to the page component as props
  };
}

const LoginTest: NextPage<{ islogin: boolean }> = (props) => {
  const [session, loading] = [{}, false];
  const queryResult = useGetUserQuery({
    variables: {
      userEmail: session?.user?.email ?? "",
    },
  });
  const { data: userData, error } = useSWR(
    "eszqsc112@gmail.com",
    async (email: string) => {
      console.log("ho!!!");
      return client.query<IGetUserQuery, IGetUserQueryVariables>({
        query: GetUserDocument,
        variables: {
          userEmail: email,
        },
      });
    }
  );

  return (
    <div>
      <div>이 메시지는 모두 볼 수 있습니다.</div>
      <div>
        <h2 className="text-2xl">세션</h2>
        <div>{session && JSON.stringify(session)}</div>
      </div>
      <div>
        <h2>로그인</h2>
        <p>
          # 로그인 했습니까? {"->"}
          {props.islogin ? "로그인했음!" : "로그인 안했음."}
        </p>
        {(!session || !session.user) && (
          <Link href="/api/auth/google/login">
            <a>로그인 고고</a>
          </Link>
        )}
      </div>
      {session?.user && !loading && (
        <div>
          <p>이 메시지는 로그인한 사용자만 볼 수 있습니다.</p>
          <div>{JSON.stringify(session.user)}</div>
          <p>당신은 로그인 하기는 했는데..</p>
          <pre>{JSON.stringify(queryResult.data?.user)}</pre>
        </div>
      )}
      <h2>유터 데이터</h2>
      <pre>{JSON.stringify(userData?.data)}</pre>
      <div>
        <h2>로그아웃</h2>
        {session && (
          <div>
            <a href="#">로그아웃</a>
          </div>
        )}
      </div>
    </div>
  );
};

export default LoginTest;
