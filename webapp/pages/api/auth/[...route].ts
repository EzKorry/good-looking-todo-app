import { NextApiRequest, NextApiResponse } from "next";
import { getLoginUrl, getMyEmail, setCredentials } from "../../../auth/google";
import { sign } from "../../../auth/jwt";
import { config } from "../../../util/config";

function isLogin(req: NextApiRequest) {
  return false;
}

export default function handler(
  req: NextApiRequest,
  res: NextApiResponse
): void {
  // console.log(req.query);
  const action = req.query.route[0];
  const provider = req.query.route[1];
  const code = req.query.code;
  // 로그인에 성공했을 때
  if (
    action === "callback" &&
    provider === "google" &&
    typeof code === "string"
  ) {
    setCredentials(code)
      .then((refresh_token) => {
        console.log(`refresh_token: ${refresh_token}`);
        const signed = sign(refresh_token);
        res.setHeader(
          "Set-Cookie",
          `${config.AUTH_COOKIE_NAME}=${signed}; Max-Age=${60 * 60}; Path=/`
        );
        return getMyEmail();
      })
      .then((value) => {
        res.json(value);
      });
  } else {
    if (isLogin(req)) res.send("success");
    else {
      res.redirect(getLoginUrl());
    }
  }
}
