import connectProxy from "next-http-proxy-middleware";
import { NextApiRequest, NextApiResponse } from "next";

export default function handler(
  req: NextApiRequest,
  res: NextApiResponse
): void {
  connectProxy(req, res, {
    changeOrigin: true,
    target: "http://localhost:4000",
    ws: true,
    pathRewrite: [
      {
        patternStr: "^/api/graphql",
        replaceStr: "/graphql",
      },
    ],
  });
}
