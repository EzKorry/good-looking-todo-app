import jwt, { JwtPayload } from "jsonwebtoken";
import { config } from "../util/config";

export type JwtVerified<DataType = string> = JwtPayload & { data: DataType };

export function sign(payload: string): string {
  return jwt.sign(
    {
      exp: Math.floor(Date.now() / 1000 + 30 * 60),
      data: payload,
    },
    config.JWT_SECRET
  );
}

export async function verify<DataType = string>(
  token: string
): Promise<JwtVerified<DataType>> {
  return new Promise<JwtVerified<DataType>>((resolve, reject) => {
    jwt.verify(token, config.JWT_SECRET, (err, decoded) => {
      if (err) {
        reject(err);
      } else if (!decoded) {
        reject(new Error("verify: cannot find decode"));
      } else {
        resolve(decoded as JwtVerified<DataType>);
      }
    });
  });
}
