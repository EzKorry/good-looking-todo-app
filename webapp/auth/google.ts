import { google } from "googleapis";
import { config } from "../util/config";

// google.people({
//   method: "GET",
//   auth: oauth2Client,
//   version: "v1",
//   resourceName: "people/me",
//   personFields: "emailAddresses",
// });

const oauth2Client = new google.auth.OAuth2(
  config.AUTH_GOOGLE_ID,
  config.AUTH_GOOGLE_SECRET,
  "http://localhost:3000/api/auth/callback/google"
);

/**
 * 구글 로그인 url를 구하는 함수
 * @returns google login url
 */
export function getLoginUrl(): string {
  return oauth2Client.generateAuthUrl({
    access_type: "offline",
    scope: [
      "https://www.googleapis.com/auth/userinfo.email",
      "https://www.googleapis.com/auth/userinfo.profile",
    ],
  });
}

const oauth2 = google.oauth2("v2");

export async function setCredentials(code: string): Promise<string> {
  const { tokens } = await oauth2Client.getToken(code);
  oauth2Client.setCredentials(tokens);
  if (typeof tokens.refresh_token !== "string") {
    throw new Error("cannot get refresh token");
  }
  return tokens.refresh_token;
}

export async function getMyEmail() {
  // return people.people.get({
  //   resourceName: "people/me",
  //   personFields: "emailAddress",
  //   auth: oauth2Client,
  // });
  return oauth2.userinfo.get({
    auth: oauth2Client,
  });
}

// export async function getUserinfo() {
//   oauth2Client.
// }
