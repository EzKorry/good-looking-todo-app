import dotenv from "dotenv";

dotenv.config();

export const config = {
  JWT_SECRET:
    process.env.JWT_SECRET ||
    "182347812364817236498256439857283495731094581324",
  IS_PRODUCTION: process.env.NODE_ENV === "production",
  AUTH_GOOGLE_ID: process.env.AUTH_GOOGLE_ID || "",
  AUTH_GOOGLE_SECRET: process.env.AUTH_GOOGLE_SECRET || "",
  HOST: process.env.HOST || "localhost",
  PORT: process.env.PORT || "3000",
  AUTH_COOKIE_NAME: process.env.AUTH_COOKIE_NAME || "x.auth",
} as const;
