use juniper::{
    graphql_object, EmptySubscription, FieldResult, GraphQLEnum, GraphQLInputObject, GraphQLObject,
    RootNode,
};

use futures::executor::block_on;
use futures::stream::TryStreamExt;
use mongodb::Client;
use mongodb::bson::doc;
use mongodb::bson::Document;

use serde::{Deserialize, Serialize};

use self::object_id::ObjectId;

mod object_id;

#[derive(GraphQLEnum, Debug, Deserialize, Serialize)]
enum UserRole {
    Admin,
    Member,
}

#[derive(GraphQLObject, Debug, Serialize)]
#[graphql(description = "앱을 사용하는 사용자입니다.")]
struct User {
    email: String,
    nickname: String,
    consent: bool,
    role: UserRole,
}

#[derive(GraphQLObject, Debug, Deserialize)]
struct UserMongoDB {
    _id: ObjectId,
    email: String,
    nickname: String,
    consent: bool,
    role: UserRole,
}

#[derive(GraphQLInputObject)]
#[graphql(description = "사용자 생성 입력")]
struct NewUserInput {
    email: String,
    nickname: String,
    consent: bool,
    role: UserRole,
}

#[derive(GraphQLEnum, Debug, Deserialize, Serialize)]
enum Episode {
    NewHope,
    Empire,
    Jedi,
}

pub struct QueryRoot {
    client: Client,
}

pub struct UserFilter {
    email: String,
}

impl From<UserFilter> for Option<Document> {
    fn from(user_filter: UserFilter) -> Self {
        Some(doc! { "email" : user_filter.email })
    }
}

#[graphql_object]
impl QueryRoot {
    fn user(&self, email: String) -> FieldResult<Option<UserMongoDB>> {
        block_on(async move {
            let collection = self
                .client
                .database("test")
                .collection::<UserMongoDB>("user");
            let mut found = collection.find(UserFilter { email }, None).await?;
            let ret = found.try_next().await?;
            Ok(ret)
        })
    }
}

pub struct MutationRoot {
    client: Client,
}

#[graphql_object]
impl MutationRoot {
    fn create_user(&self, new_user: NewUserInput) -> FieldResult<User> {
        block_on(async move {
            let collection = self.client.database("test").collection::<User>("user");
            let user = User {
                email: new_user.email,
                nickname: (new_user.nickname),
                consent: (new_user.consent),
                role: (new_user.role),
            };
            collection.insert_one(&user, None).await?;
            Ok(user)
        })
    }
}

pub type Schema = RootNode<'static, QueryRoot, MutationRoot, EmptySubscription>;

pub fn create_schema(client: Client) -> Schema {
    Schema::new(
        QueryRoot {
            client: client.clone(),
        },
        MutationRoot { client },
        EmptySubscription::new(),
    )
}
