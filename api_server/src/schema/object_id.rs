use juniper::{graphql_scalar, ParseScalarResult, ParseScalarValue, Value};
use std::str::FromStr;

use serde::Deserialize;

use mongodb::bson::oid::ObjectId as MongodbObjectId;
#[derive(Debug, Deserialize)]
pub struct ObjectId(MongodbObjectId);

impl ToString for ObjectId {
    fn to_string(&self) -> String {
        self.0.to_string()
    }
}

impl FromStr for ObjectId {
    type Err = mongodb::bson::oid::Error;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        Ok(ObjectId(MongodbObjectId::from_str(s)?))
    }
}

#[graphql_scalar(description = "Mongodb ObjectId")]
impl<S> GraphQLScalar for ObjectId
where
    S: ScalarValue,
{
    // Define how to convert your custom scalar into a primitive type.
    fn resolve(&self) -> Value {
        Value::scalar(self.to_string())
    }

    // Define how to parse a primitive type into your custom scalar.
    fn from_input_value(v: &InputValue) -> Option<ObjectId> {
        v.as_scalar_value()
            .and_then(|v| v.as_str())
            .and_then(|s| s.parse().ok())
    }

    // Define how to parse a string value.
    fn from_str<'a>(value: ScalarToken<'a>) -> ParseScalarResult<'a, S> {
        <String as ParseScalarValue<S>>::from_str(value)
    }
}
