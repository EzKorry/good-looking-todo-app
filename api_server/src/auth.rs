use std::io::Error;

use futures::Future;
use oauth2::basic::BasicClient;
use oauth2::reqwest::async_http_client;
use oauth2::{AuthUrl, AuthorizationCode, ClientId, ClientSecret, CsrfToken, PkceCodeChallenge, PkceCodeVerifier, RedirectUrl, Scope, TokenResponse, TokenUrl};
use url::Url;
use actix_web::HttpResponse;

#[derive(Debug, Clone)]
pub struct Authinfo {
    pub client_id: String,
    pub client_secret: String,
    pub auth_url: String,
    pub token_url: String,
    pub redirect_url: String,
}

// pub fn create_handler(
//     authinfo: &Authinfo,
// ) -> (Box<dyn FnOnce() -> Result<HttpResponse, url::ParseError>>, Box<dyn FnOnce() -> dyn Future<Output = Result<HttpResponse, url::ParseError>>>) {
//     let cloned = authinfo.clone();
//     let (auth_url, csrf_token) = get_auth_url(cloned).unwrap();
//     (Box::new(async || { 
        
//         Ok(HttpResponse::Ok().finish())
//     }), Box::new(async || { 
        
//         Ok(HttpResponse::Ok().finish())
//     }))
// }

pub fn get_auth_url(authinfo: Authinfo) -> Result<(Url, CsrfToken), url::ParseError> {
    // Create an OAuth2 client by specifying the client ID, client secret, authorization URL and
    // token URL.
    let client = BasicClient::new(
        ClientId::new(authinfo.client_id.to_string()),
        Some(ClientSecret::new(authinfo.client_secret.to_string())),
        AuthUrl::new(authinfo.auth_url.to_string())?,
        Some(TokenUrl::new(authinfo.token_url.to_string())?),
    )
    // Set the URL the user will be redirected to after the authorization process.
    .set_redirect_uri(RedirectUrl::new(authinfo.redirect_url.to_string())?);

    // Generate a PKCE challenge.
    // let (pkce_challenge, _pkce_verifier) = PkceCodeChallenge::new_random_sha256();

    // Generate the full authorization URL.
    let (auth_url, csrf_token) = client
        .authorize_url(CsrfToken::new_random)
        // Set the desired scopes.
        .add_scope(Scope::new("https://www.googleapis.com/auth/userinfo.email".to_string()))
        .add_scope(Scope::new("https://www.googleapis.com/auth/userinfo.profile".to_string()))
        .add_extra_param("access_type", "offline")
        // Set the PKCE code challenge.
        // .set_pkce_challenge(pkce_challenge)
        .url();
    Ok((auth_url, csrf_token))

    // This is the URL you should redirect the user to, in order to trigger the authorization
    // process.
    // println!("Browse to: {}", auth_url);

    // // Once the user has been redirected to the redirect URL, you'll have access to the
    // // authorization code. For security reasons, your code should verify that the `state`
    // // parameter returned by the server matches `csrf_state`.

    // // Now you can trade it for an access token.
    // let token_result = client
    //     .exchange_code(AuthorizationCode::new(
    //         "some authorization code".to_string(),
    //     ))
    //     // Set the PKCE code verifier.
    //     .set_pkce_verifier(pkce_verifier)
    //     .request_async(async_http_client)
    //     .await
    //     .unwrap();
    // Ok(())
    // // Unwrapping token_result will either produce a Token or a RequestTokenError.
}
