use futures::executor::block_on;
use serde::Deserialize;
use tokio::fs::File;
use tokio::io::{AsyncReadExt, BufReader};

#[derive(Debug, Deserialize, Clone)]
pub struct Config {
  pub test_mongo_url: String,
  pub database_name: String,
  pub client_id: String,
  pub client_secret: String,
}

// TODO: cached

pub fn get_config() -> Config {
    // let config_path = RelativePath::new("../config.json");
    
    let file_result = block_on(async {
        File::open("config.json").await
    });
    let file = match file_result {
        Ok(file) => file,
        Err(error) => panic!(
            "There's no config file. please create config.json on your working directory. {:?}",
            error
        ),
    };
    let mut reader = BufReader::new(file);
    let buffer = block_on(async {
        let mut buffer = String::new();
        reader.read_to_string(&mut buffer).await.unwrap();
        buffer
    });
    let result = serde_json::from_str::<Config>(&buffer);
    match result {
        Ok(data) => data,
        Err(error) => panic!("wrong json format: {:?}", error),
    }
}
