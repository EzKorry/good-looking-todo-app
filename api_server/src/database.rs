use crate::env::Config;

async fn print_db_name(client: &Client) -> mongodb::error::Result<()> {
    for db_name in client.list_database_names(None, None).await? {
        println!("{}", db_name);
    }
    Ok(())
}

use mongodb::{options::ClientOptions, Client};
pub async fn client(config: &Config) -> Client {
    let mut client_options = ClientOptions::parse(&config.test_mongo_url).await.unwrap();
    client_options.app_name = Some("Good-Looking-Todo-App Api Server".to_string());
    let client = Client::with_options(client_options).unwrap();
    print_db_name(&client).await.unwrap();
    client
}
