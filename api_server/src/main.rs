use auth::{Authinfo, get_auth_url};
use env::get_config;
use juniper::http::GraphQLRequest;
use mongodb::Client;
use oauth2::CsrfToken;
use schema::Schema;
use url::Url;

use std::{io, sync::Arc};
use serde::Deserialize;

use actix_cors::Cors;
use actix_web::{App, Error, HttpRequest, HttpResponse, HttpServer, middleware, web};

mod auth;
mod database;
mod env;
mod graphql;
mod schema;

use crate::{
    database::client,
    graphql::{graphiql, graphql},
    schema::create_schema,
    env::Config
};

mod data_structure;

pub async fn to_naver() -> Result<HttpResponse, Error> {
    Ok(HttpResponse::Found().header("Location", "https://naver.com").finish())
}

#[tokio::main]
async fn main() -> mongodb::error::Result<()> {
    // 데이터베이스 초기화
    let config = get_config();
    let client = client(&config).await;
    

    // 웹앱 만들기 (데이터베이스 클라이언트 넣기)

    // 웹앱 구동
    // let coll = client.database("test").collection::<Document>("man");
    // coll.insert_one(doc! { "name": "hello" }, None).await?;
    // let mut found = coll.find(None, None).await?;
    // while let Some(doc) = found.next().await {
    //     println!("{}", doc?)
    // }
    webapp(client, config.clone())?;
    Ok(())
}

async fn test_authinfo(auth_url: web::Data<(Url, CsrfToken)>) -> Result<HttpResponse, Error> {
    Ok(HttpResponse::Found().header("Location",auth_url.0.to_string()).finish())
}

#[derive(Debug, Deserialize)]
struct Info {
    code: Option<String>,
}

async fn google_callback(info: web::Query<Info>) -> Result<HttpResponse, Error> {
    println!("{:?}", info);
    Ok(HttpResponse::Ok().body("hello"))
}

#[actix_web::main]
async fn webapp(database_client: Client, config: Config) -> io::Result<()> {
    std::env::set_var("RUST_LOG", "actix_web=info");
    env_logger::init();

    let authinfo = Authinfo {
        auth_url: "https://accounts.google.com/o/oauth2/v2/auth".to_string(),
        client_id: config.client_id.clone(),
        client_secret: config.client_secret.clone(),
        redirect_url: "http://localhost:4000/api/auth/callback/google".to_string(),
        token_url: "https://oauth2.googleapis.com/token".to_string(),
    };
    // let (auth_check_handler, login_handler) = create_handler(&authinfo);

    let auth_toolkit = get_auth_url(authinfo).unwrap();
    

    // Create Juniper schema
    let schema = std::sync::Arc::new(create_schema(database_client));

    // Start http server
    HttpServer::new(move || {
        App::new()
            .data(schema.clone())
            .data(auth_toolkit.clone())
            .wrap(middleware::Logger::default())
            .wrap(
                Cors::new()
                    .allowed_methods(vec!["POST", "GET"])
                    .supports_credentials()
                    .allowed_origin("localhost:3000")
                    .max_age(3600)
                    .finish(),
            )
            .service(web::resource("/graphql").route(web::post().to(graphql)))
            .service(web::resource("/graphiql").route(web::get().to(graphiql)))
            .service(web::resource("/to_naver").route(web::get().to(to_naver)))
            .service(web::resource("/api/auth").route(web::get().to(test_authinfo)))
            .service(web::resource("/api/auth/callback/google").route(web::get().to(google_callback)))
    })
    .bind("127.0.0.1:4000")?
    .run()
    .await
}
